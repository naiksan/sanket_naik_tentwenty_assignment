package com.naiksan.bookmovies.data.repository

import androidx.lifecycle.LiveData
import com.naiksan.bookmovies.data.model.Card

interface CardRepository {
    suspend fun getSavedCard(): LiveData<Card>
}