package com.naiksan.bookmovies.ui.payment

import androidx.lifecycle.ViewModel
import com.naiksan.bookmovies.data.repository.CardRepository
import com.naiksan.bookmovies.data.repository.MoviesRepository
import com.naiksan.bookmovies.data.repository.SeatsRepository
import com.naiksan.bookmovies.internal.lazyDeferred

class CardDetailsViewModel(
    private val cardRepository: CardRepository
) : ViewModel() {
    val card by lazyDeferred {
        cardRepository.getSavedCard()
    }
}