package com.naiksan.bookmovies.ui.movies


import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.naiksan.bookmovies.R

import com.naiksan.bookmovies.adapter.MovieListAdapter
import com.naiksan.bookmovies.data.model.Movie
import com.naiksan.bookmovies.databinding.FragmentMovieListBinding
import com.naiksan.bookmovies.ui.base.ScopedFragment
import com.naiksan.bookmovies.ui.base.SharedViewModel
import com.naiksan.bookmovies.ui.util.SelectSeatCountDialog
import com.naiksan.bookmovies.util.setGrid
import kotlinx.android.synthetic.main.fragment_movie_list.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class MovieListFragment : ScopedFragment(), KodeinAware {

    override val kodein by kodein()
    private val viewModelFactory: MovieListViewModelFactory by instance()
    private lateinit var viewModel: MovieListViewModel
    private lateinit var sharedViewModel: SharedViewModel
    private lateinit var adapter: MovieListAdapter
    private lateinit var binding: FragmentMovieListBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentMovieListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MovieListViewModel::class.java)
        sharedViewModel = ViewModelProviders.of(requireActivity()).get(SharedViewModel::class.java)
        bindUI()
    }

    private fun bindUI() {
        adapter = MovieListAdapter(listener)
        list.setGrid(requireContext(), 2)
        list.adapter = adapter
        swipeRefresh.setOnRefreshListener { fetchMovies() }
        fetchMovies()
    }

    private fun fetchMovies() {
        launch {
            showLoading()
            val movies = viewModel.movies.await()
            movies.observe(this@MovieListFragment, Observer { list ->
                hideLoading()
                if (list == null) {
                    binding.showError = true
                    binding.errorMessage = "Some error occurred"
                    return@Observer
                }
                binding.showError = true
                adapter.submitList(list)
            })
        }
    }

    private fun showLoading() {
        swipeRefresh.isRefreshing = true
        binding.isLoading = true
    }

    private fun hideLoading() {
        swipeRefresh.isRefreshing = false
        binding.isLoading = false
    }

    private val listener = object : MovieListAdapter.Listener {
        override fun onItemClick(movie: Movie) {

        }

        override fun onBookClick(movie: Movie) {

            sharedViewModel.selectMovie(movie)
            MovieListFragmentDirections.actionFragmentMovieListToFragmentBookTicket().let {
                findNavController().navigate(it)
            }
        }
    }
}
