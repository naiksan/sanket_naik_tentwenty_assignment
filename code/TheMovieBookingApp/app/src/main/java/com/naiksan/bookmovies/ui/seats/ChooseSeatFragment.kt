package com.naiksan.bookmovies.ui.seats

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DefaultItemAnimator
import com.naiksan.bookmovies.adapter.SeatListAdapter
import com.naiksan.bookmovies.data.model.Seat
import com.naiksan.bookmovies.ui.base.ScopedFragment
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import com.naiksan.bookmovies.databinding.FragmentChooseSeatsBinding
import com.naiksan.bookmovies.ui.base.SharedViewModel
import com.naiksan.bookmovies.util.Constants
import kotlinx.android.synthetic.main.fragment_choose_seats.*
import kotlinx.coroutines.launch
import org.kodein.di.generic.instance

class ChooseSeatFragment : ScopedFragment(), KodeinAware {

    override val kodein by kodein()
    private lateinit var viewModel: SeatListViewModel
    private lateinit var sharedViewModel: SharedViewModel
    private val viewModelFactory: SeatListViewModelFactory by instance()
    private lateinit var binding: FragmentChooseSeatsBinding
    private lateinit var adapter: SeatListAdapter
    private val args: ChooseSeatFragmentArgs by navArgs()
    private var seats: ArrayList<Seat> = ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentChooseSeatsBinding.inflate(inflater, container, false)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SeatListViewModel::class.java)
        sharedViewModel = ViewModelProviders.of(requireActivity()).get(SharedViewModel::class.java)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        bindUI()
    }

    override fun onPause() {
        super.onPause()
    }

    private val listener = object : SeatListAdapter.Listener {
        override fun onItemClick(pos: Int) {
            val seatsSelected: Int = seats.filter { it.state == Constants.Seat.SELECTED }.count()
            val seat = seats[pos]
            if (seat.state == Constants.Seat.AVAILABLE && seatsSelected < args.seatCount) {
                seat.state = Constants.Seat.SELECTED
                adapter.notifyItemChanged(pos)
                binding.showProceed = seatsSelected == args.seatCount - 1
            } else if (seat.state == Constants.Seat.SELECTED) {
                seat.state = Constants.Seat.AVAILABLE
                adapter.notifyItemChanged(pos)
                binding.showProceed = false
            }
        }
    }

    private fun bindUI() {

        binding.onProceed = View.OnClickListener {
            sharedViewModel.selectSeat(adapter.getSelectedNumbers())
            ChooseSeatFragmentDirections.actionFragmentChooseSeatToFragmentCardDetails().let {
                findNavController().navigate(it)
            }
        }
        adapter = SeatListAdapter(seats, listener)

        list.itemAnimator = DefaultItemAnimator()
        list.adapter = adapter
        getSeats()
    }

    private fun getSeats() {
        launch {
            viewModel.seats.await().observe(this@ChooseSeatFragment, Observer { list ->
                seats.clear()
                seats.addAll(list)
                adapter.notifyDataSetChanged()
                val seatsSelected: Int = seats.filter { it.state == Constants.Seat.SELECTED }.count()
                binding.showProceed = seatsSelected == args.seatCount
            })
        }
    }
}