package com.naiksan.bookmovies.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.naiksan.bookmovies.data.model.Movie
import com.naiksan.bookmovies.data.model.Seat

@Dao
interface MoviesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(movie: Movie)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(movies: List<Movie>)

    @Query("SELECT * FROM movies")
    fun get(): LiveData<List<Movie>>

}