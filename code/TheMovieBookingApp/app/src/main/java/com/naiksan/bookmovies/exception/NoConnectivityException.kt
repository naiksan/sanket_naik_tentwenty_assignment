package com.naiksan.bookmovies.exception

import java.io.IOException

class NoConnectivityException: IOException()