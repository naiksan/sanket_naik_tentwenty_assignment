package com.naiksan.bookmovies.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.naiksan.bookmovies.data.model.Movie
import com.naiksan.bookmovies.databinding.ItemMovieBinding

class MovieListAdapter(
    private val listener: Listener
) : ListAdapter<Movie, MovieListAdapter.MyViewHolder>(MovieDiffCallback()) {
    companion object {
        val TAG = MovieListAdapter::class.java.simpleName
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            ItemMovieBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(createItemClickListener(position), createBookClickListener(position), item)
    }

    private fun createItemClickListener(pos: Int): View.OnClickListener = View.OnClickListener { view ->
        listener.onItemClick(getItem(pos))
    }

    private fun createBookClickListener(pos: Int): View.OnClickListener = View.OnClickListener { view ->
        listener.onBookClick(getItem(pos))
    }

    class MyViewHolder(
        val binding: ItemMovieBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(listener1: View.OnClickListener, listener2: View.OnClickListener, itm: Movie) {
            binding.apply {
                itemClickListener = listener1
                bookTicketListener = listener2
                movie = itm
                executePendingBindings()
            }
        }
    }

    interface Listener {
        fun onItemClick(movie: Movie)
        fun onBookClick(movie: Movie)
    }

    class MovieDiffCallback : DiffUtil.ItemCallback<Movie>() {
        override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
            return oldItem == newItem
        }

    }
}