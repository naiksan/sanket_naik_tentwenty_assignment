package com.naiksan.bookmovies.data.repository

import androidx.lifecycle.LiveData
import com.naiksan.bookmovies.data.db.dao.CardDao
import com.naiksan.bookmovies.data.model.Card

class CardRepositoryImpl(
    private val cardDao: CardDao
) : CardRepository {
    override suspend fun getSavedCard(): LiveData<Card> {
        return cardDao.get()
    }
}