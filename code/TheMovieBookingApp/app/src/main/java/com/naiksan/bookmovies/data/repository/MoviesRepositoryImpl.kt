package com.naiksan.bookmovies.data.repository

import androidx.lifecycle.LiveData
import com.naiksan.bookmovies.data.db.dao.MoviesDao
import com.naiksan.bookmovies.data.model.Movie
import com.naiksan.bookmovies.data.network.MoviesDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MoviesRepositoryImpl(
    private val moviesDao: MoviesDao,
    private val moviesDataSource: MoviesDataSource
) : MoviesRepository {
    override suspend fun getUpcomingMovies(): LiveData<List<Movie>> {
        return withContext(Dispatchers.IO) {
            initUpcomingMovies()
            return@withContext moviesDao.get()
        }
    }

    init {
        moviesDataSource.downloadedMovies.observeForever{list->
            save(list)
        }
    }

    private fun save(movies: List<Movie>?) {
        GlobalScope.launch(Dispatchers.IO){
            movies?.let {
                moviesDao.save(it)
            }
        }
    }

    private suspend fun initUpcomingMovies() {
        moviesDataSource.fetchUpcomingMovies()
    }
}