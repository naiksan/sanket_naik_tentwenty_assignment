package com.naiksan.bookmovies.ui.payment

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.naiksan.bookmovies.data.repository.CardRepository
import com.naiksan.bookmovies.data.repository.SeatsRepository

class CardDetailsViewModelFactory(
    private val cardRepository: CardRepository
) : ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CardDetailsViewModel(cardRepository) as T
    }
}