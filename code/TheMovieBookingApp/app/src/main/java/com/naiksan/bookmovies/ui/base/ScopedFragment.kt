package com.naiksan.bookmovies.ui.base

import android.os.Bundle
import android.view.Menu
import androidx.fragment.app.Fragment
import com.naiksan.bookmovies.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

abstract  class ScopedFragment : Fragment(), CoroutineScope{
    private lateinit var job: Job

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        job = Job()
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        menu?.findItem(R.id.info)?.setVisible(false)
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }
}