package com.naiksan.bookmovies.workers

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.naiksan.bookmovies.data.db.AppDatabase
import com.naiksan.bookmovies.data.model.Cinema
import com.naiksan.bookmovies.data.model.Location
import com.naiksan.bookmovies.data.model.Seat
import com.naiksan.bookmovies.data.model.ShowTime
import com.naiksan.bookmovies.util.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope

class SeedDatabaseWorker(
    context: Context,
    workerParams: WorkerParameters
) : CoroutineWorker(context, workerParams) {

    private val TAG by lazy { SeedDatabaseWorker::class.java.simpleName }
    override val coroutineContext = Dispatchers.IO

    override suspend fun doWork(): Result = coroutineScope {
        val seats: List<Seat> = getList()

        val database = AppDatabase(applicationContext)
        database.seatDao().save(seats)

        database.locationDao().save(createLocationList())
        database.cinemaDao().save(createCinemaList())
        database.showTimeDao().save(createShowList())
        Result.success()
    }

    //seed location table
    private fun createLocationList(): List<Location> {
        return listOf(
            Location(0, "Panaji"),
            Location(0, "Porvorim"),
            Location(0, "Bicholim")
        )
    }

    //seed cinema table
    private fun createCinemaList(): List<Cinema> {
        return listOf(
            Cinema(0, 1, "Inox"),
            Cinema(0, 2, "Inox"),
            Cinema(0, 3, "Heera talkies")
        )
    }

    //seed shows table
    private fun createShowList(): List<ShowTime> {
        return listOf(
            ShowTime(0, 1, "9:00 AM"),
            ShowTime(0, 1, "11:00 AM"),
            ShowTime(0, 1, "2:30AM"),
            ShowTime(0, 2, "9:00 AM"),
            ShowTime(0, 2, "11:00 AM"),
            ShowTime(0, 3, "2:30AM")
        )
    }


    //seed seat layout
    private fun getList(): List<Seat> {
        val list: ArrayList<Seat> = ArrayList()
        for (i in 1..8)
            list.add(Seat(0, "A$i", Constants.Seat.AVAILABLE))
        for (i in 1..3)
            list.add(Seat(0, "B$i", Constants.Seat.BOOKED))
        for (i in 4..8)
            list.add(Seat(0, "B$i", Constants.Seat.AVAILABLE))
        for (i in 1..8)
            list.add(Seat(0, "C$i", Constants.Seat.AVAILABLE))
        for (i in 1..8)
            list.add(Seat(0, "D$i", Constants.Seat.AVAILABLE))
        for (i in 1..8)
            list.add(Seat(0, "E$i", Constants.Seat.AVAILABLE))
        for (i in 1..4)
            list.add(Seat(0, "F$i", Constants.Seat.BOOKED))
        for (i in 5..6)
            list.add(Seat(0, "F$i", Constants.Seat.BOOKED))
        for (i in 7..8)
            list.add(Seat(0, "F$i", Constants.Seat.BOOKED))
        for (i in 1..7)
            list.add(Seat(0, "G$i", Constants.Seat.BOOKED))
        list.add(Seat(0, "G8", Constants.Seat.AVAILABLE))
        return list
    }
}