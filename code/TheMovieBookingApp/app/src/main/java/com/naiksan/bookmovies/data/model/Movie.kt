package com.naiksan.bookmovies.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.naiksan.bookmovies.util.toDisplayDate

@Entity(tableName = "movies")
data class Movie(
    @SerializedName("id")
    @PrimaryKey
    @ColumnInfo(name = "movie_id")
    val id: Int,
    @SerializedName("vote_count")
    val voteCount: Int,
    @SerializedName("video")
    val video: Boolean,
    @SerializedName("vote_average")
    val voteAverage: Double,
    @SerializedName("title")
    val title: String,
    @SerializedName("popularity")
    val popularity: Double,
    @SerializedName("poster_path")
    val posterPath: String,
    @SerializedName("original_language")
    val originalLanguage: String,
    @SerializedName("original_title")
    val originalTitle: String,
    @SerializedName("backdrop_path")
    val backdropPath: String,
    @SerializedName("adult")
    val adult: Boolean,
    @SerializedName("overview")
    val overview: String,
    @SerializedName("release_date")
    val releaseDate: String
) {
    val releaseDateDisplay: String
        get() = releaseDate.toDisplayDate()
}
