package com.naiksan.bookmovies.util

import android.text.TextUtils

object Validations {
    fun isCardNumberValid(value: String): Boolean {
        return value.length == 16
    }

    fun isExpiryMonthValid(value: String): Boolean {
        if (value.length != 2) return false
        value.toInt().let {
            if (it > 12 || it < 0) return false
        }
        return true
    }

    fun isExpiryYearValid(value: String): Boolean  = value.length == 2

    fun isCvvValid(value: String): Boolean = value.length == 3

    fun isHolderNameValid(value: String): Boolean = !TextUtils.isEmpty(value)
}