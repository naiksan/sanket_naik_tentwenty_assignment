package com.naiksan.bookmovies.data.network

import androidx.lifecycle.LiveData
import com.naiksan.bookmovies.data.model.Movie

interface MoviesDataSource {
    val downloadedMovies: LiveData<List<Movie>>

    suspend fun fetchUpcomingMovies()
}