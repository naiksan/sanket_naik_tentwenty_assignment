package com.naiksan.bookmovies.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.naiksan.bookmovies.data.model.Seat

@Dao
interface SeatDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(seat: Seat)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(seats: List<Seat>)

    @Query("SELECT * FROM seats")
    fun get(): LiveData<List<Seat>>

}