package com.naiksan.bookmovies.data.repository

import androidx.lifecycle.LiveData
import com.naiksan.bookmovies.data.model.Movie

interface MoviesRepository {
    suspend fun getUpcomingMovies(): LiveData<List<Movie>>
}