package com.naiksan.bookmovies.util

import com.naiksan.bookmovies.BuildConfig

object Constants {
    const val DB_NAME: String = "tmdbmovies.db"

    object Service {
        const val BASE_URL = BuildConfig.SERVER_BASE_URL
        const val API_KEY = BuildConfig.API_KEY
        const val CONNECT: Long = 30 * 1000
        const val READ: Long = 30 * 1000
        const val WRITE: Long = 30 * 1000
    }

    object Seat {
        const val AVAILABLE = "AVAILABLE"
        const val SELECTED = "SELECTED"
        const val BOOKED = "BOOKED"
    }
}