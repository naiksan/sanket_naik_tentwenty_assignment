package com.naiksan.bookmovies.data.network

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.naiksan.bookmovies.data.model.Movie
import com.naiksan.bookmovies.exception.NoConnectivityException
import com.naiksan.bookmovies.network.service.ApiService
import retrofit2.HttpException

class MoviesDataSourceImpl(
    private val apiService: ApiService
) : MoviesDataSource {

    private val _downloadedMovies = MutableLiveData<List<Movie>>()

    override val downloadedMovies: LiveData<List<Movie>>
        get() = _downloadedMovies

    override suspend fun fetchUpcomingMovies() {
        try {
            val response = apiService
                .getUpcomingMoviesAsync()
                .await()
            _downloadedMovies.postValue(response.results)
        } catch (e: NoConnectivityException) {
            Log.e("fetchUpcomingMovies", "Network error occurred", e)
        } catch (e: HttpException) {
            Log.e("fetchUpcomingMovies", "HttpException", e)
        }
    }
}