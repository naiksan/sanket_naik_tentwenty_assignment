package com.naiksan.bookmovies.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "cinema")
data class Cinema(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "cinema_id")
    val id: Int,
    val locationId: Int,
    @ColumnInfo(name = "cinema_name")
    val name: String
)