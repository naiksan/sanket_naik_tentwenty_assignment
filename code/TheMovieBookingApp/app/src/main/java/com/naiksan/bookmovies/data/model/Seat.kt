package com.naiksan.bookmovies.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.naiksan.bookmovies.util.Constants

@Entity(tableName = "seats")
data class Seat(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "seat_id")
    val id: Int,
    val seatNo: String,
    var state: String = Constants.Seat.AVAILABLE
)