package com.naiksan.bookmovies.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.naiksan.bookmovies.databinding.ItemSeatCountBinding
import com.naiksan.bookmovies.ui.movies.MovieListFragmentDirections

class SeatCountAdapter(private val listener: (count: Count) -> Unit) :
    RecyclerView.Adapter<SeatCountAdapter.MyViewHolder>() {

    private val counts: List<Count>   by lazy { createList() }

    private fun createList(): List<Count> {
        val counts: ArrayList<Count> = ArrayList()
        for (i in 1..8) {
            counts.add(Count(i))
        }
        return counts
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            ItemSeatCountBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }


    override fun getItemCount(): Int = counts.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(createItemClickListener(position), item)
    }

    private fun getItem(position: Int): Count = counts[position]

    private fun createItemClickListener(pos: Int): View.OnClickListener = View.OnClickListener { view ->
        listener(counts[pos])
    }

    class MyViewHolder(
        val binding: ItemSeatCountBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(listener: View.OnClickListener, itm: Count) {
            binding.apply {
                itemClickListener = listener
                count = itm
                executePendingBindings()
            }
        }
    }

    data class Count(val count: Int)
}