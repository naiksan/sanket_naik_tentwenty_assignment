package com.naiksan.bookmovies.ui.book

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.naiksan.bookmovies.data.repository.BookTicketRepository
import com.naiksan.bookmovies.data.repository.MoviesRepository

class BookTicketViewModelFactory(
    private val bookTicketRepository: BookTicketRepository
) : ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return BookTicketViewModel(bookTicketRepository) as T
    }
}