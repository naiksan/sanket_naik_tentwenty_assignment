package com.naiksan.bookmovies.data.repository

import androidx.lifecycle.LiveData
import com.naiksan.bookmovies.data.db.dao.CinemaDao
import com.naiksan.bookmovies.data.db.dao.LocationDao
import com.naiksan.bookmovies.data.db.dao.ShowTimeDao
import com.naiksan.bookmovies.data.model.Cinema
import com.naiksan.bookmovies.data.model.Location
import com.naiksan.bookmovies.data.model.ShowTime
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class BookTicketRepositoryImpl(
    private val locationDao: LocationDao,
    private val cinemaDao: CinemaDao,
    private val showTimeDao: ShowTimeDao
) : BookTicketRepository {
    override suspend fun getLocations(): LiveData<List<Location>> {
        return withContext(Dispatchers.IO) {
            return@withContext locationDao.get()
        }
    }

    override suspend fun getCinemas(locationId: Int): LiveData<List<Cinema>> {
        return withContext(Dispatchers.IO) {
            return@withContext cinemaDao.get(locationId)
        }
    }

    override suspend fun getShowTime(cinemaId: Int): LiveData<List<ShowTime>> {
        return withContext(Dispatchers.IO) {
            return@withContext showTimeDao.get(cinemaId)
        }
    }
}