package com.naiksan.bookmovies.ui.util

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.view.View
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import com.naiksan.bookmovies.R
import com.naiksan.bookmovies.adapter.SeatCountAdapter
import com.naiksan.bookmovies.internal.interfaces.BottomSheetOnStateChanged
import kotlinx.android.synthetic.main.dialog_seat_count.view.*


class SelectSeatCountDialog(private val listener: (count: SeatCountAdapter.Count)->Unit) : BottomSheetDialogFragment() {

    private val onStateChangedCallback = BottomSheetOnStateChanged { _, newState ->
        if (newState == BottomSheetBehavior.STATE_HIDDEN) {
            dismiss()
        }
    }

    private lateinit var ctx: Context

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        ctx = activity ?: return

        val contentView = View.inflate(context, R.layout.dialog_seat_count, null)
        dialog.setContentView(contentView)
        val params = (contentView.parent as View).layoutParams as CoordinatorLayout.LayoutParams
        val behavior = params.behavior
        if (behavior != null && behavior is BottomSheetBehavior) {
            behavior.setBottomSheetCallback(onStateChangedCallback)
        }
        (contentView.parent as View).setBackgroundColor(
            ContextCompat.getColor(
                context!!,
                android.R.color.transparent
            )
        )
        initUI(contentView)
    }

    private fun initUI(contentView: View) {
        val list = contentView.list
        list.adapter = SeatCountAdapter{
            listener(it)
            dismiss()
        }
    }

}