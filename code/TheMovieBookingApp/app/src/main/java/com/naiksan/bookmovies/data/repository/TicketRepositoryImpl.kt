package com.naiksan.bookmovies.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.naiksan.bookmovies.data.model.*
import java.util.*

class TicketRepositoryImpl(
    private val selectedMovie: LiveData<Movie>,
    private val selectedCard: LiveData<Card>,
    private val selectedSeatNos: LiveData<String>,
    private val selectedCinema: LiveData<Cinema>,
    private val selectedShowTime: LiveData<ShowTime>,
    private val selectedLocation: LiveData<Location>
) : TicketRepository {
    override suspend fun getTicket(): LiveData<Ticket> {
        val ticket = MutableLiveData<Ticket>()
        ticket.value = Ticket(
            0,
            UUID.randomUUID().toString(),
            selectedSeatNos.value,
            selectedShowTime.value,
            selectedCinema.value,
            selectedLocation.value,
            selectedMovie.value,
            selectedCard.value
        )
        return ticket
    }
}