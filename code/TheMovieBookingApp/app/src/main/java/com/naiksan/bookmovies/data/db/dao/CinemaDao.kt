package com.naiksan.bookmovies.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.naiksan.bookmovies.data.model.Cinema
import com.naiksan.bookmovies.data.model.ShowTime

@Dao
interface CinemaDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(cinema: Cinema)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(cinema: List<Cinema>)

    @Query("SELECT * FROM cinema")
    fun get(): LiveData<List<Cinema>>

    @Query("SELECT * FROM cinema WHERE locationId = :locationId")
    fun get(locationId: Int): LiveData<List<Cinema>>

}