package com.naiksan.bookmovies

import android.app.Application
import com.naiksan.bookmovies.data.db.AppDatabase
import com.naiksan.bookmovies.data.network.MoviesDataSource
import com.naiksan.bookmovies.data.network.MoviesDataSourceImpl
import com.naiksan.bookmovies.data.repository.*
import com.naiksan.bookmovies.network.service.ApiService
import com.naiksan.bookmovies.network.service.ConnectivityInterceptor
import com.naiksan.bookmovies.network.service.ConnectivityInterceptorImpl
import com.naiksan.bookmovies.ui.book.BookTicketViewModelFactory
import com.naiksan.bookmovies.ui.movies.MovieListViewModelFactory
import com.naiksan.bookmovies.ui.payment.CardDetailsViewModelFactory
import com.naiksan.bookmovies.ui.seats.SeatListViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class MovieApplication : Application(), KodeinAware {
    override val kodein: Kodein
        get() = Kodein.lazy {
            import(androidXModule(this@MovieApplication))

            bind() from singleton { AppDatabase(instance()) }
            bind() from singleton { instance<AppDatabase>().seatDao() }
            bind() from singleton { instance<AppDatabase>().movieDao() }
            bind() from singleton { instance<AppDatabase>().locationDao() }
            bind() from singleton { instance<AppDatabase>().cinemaDao() }
            bind() from singleton { instance<AppDatabase>().showTimeDao() }
            bind() from singleton { instance<AppDatabase>().cardDao() }
            bind() from singleton { instance<AppDatabase>().ticketDao() }

            bind<ConnectivityInterceptor>() with singleton { ConnectivityInterceptorImpl(instance()) }
            bind() from singleton { ApiService(instance()) }

            bind<MoviesDataSource>() with singleton { MoviesDataSourceImpl(instance()) }
            bind<MoviesRepository>() with singleton { MoviesRepositoryImpl(instance(), instance()) }
            bind() from provider { MovieListViewModelFactory(instance()) }

            bind<SeatsRepository>() with singleton { SeatsRepositoryImpl(instance()) }
            bind() from provider { SeatListViewModelFactory(instance()) }

            bind<CardRepository>() with singleton { CardRepositoryImpl(instance()) }
            bind() from provider { CardDetailsViewModelFactory(instance()) }

            bind<BookTicketRepository>() with singleton { BookTicketRepositoryImpl(instance(), instance(), instance()) }
            bind() from provider { BookTicketViewModelFactory(instance()) }
        }
}