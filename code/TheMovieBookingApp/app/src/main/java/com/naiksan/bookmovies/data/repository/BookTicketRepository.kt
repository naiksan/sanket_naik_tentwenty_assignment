package com.naiksan.bookmovies.data.repository

import androidx.lifecycle.LiveData
import com.naiksan.bookmovies.data.model.Cinema
import com.naiksan.bookmovies.data.model.Location
import com.naiksan.bookmovies.data.model.ShowTime

interface BookTicketRepository {
    suspend fun getLocations(): LiveData<List<Location>>
    suspend fun getCinemas(locationId: Int): LiveData<List<Cinema>>
    suspend fun getShowTime(cinemaId: Int): LiveData<List<ShowTime>>
}