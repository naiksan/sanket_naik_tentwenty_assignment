package com.naiksan.bookmovies.internal.interfaces

import android.text.Editable
import android.text.TextWatcher

class OnTextChanged(val textChanged: (value: Editable) -> Unit) : TextWatcher {
    override fun afterTextChanged(value: Editable) {
        textChanged(value)
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }
}