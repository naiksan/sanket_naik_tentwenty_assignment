package com.naiksan.bookmovies.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.naiksan.bookmovies.data.model.ShowTime

@Dao
interface ShowTimeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(time: ShowTime)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(time: List<ShowTime>)

    @Query("SELECT * FROM showtime")
    fun get(): LiveData<List<ShowTime>>

    @Query("SELECT * FROM showtime WHERE cinemaId = :cinemaId")
    fun get(cinemaId: Int): LiveData<List<ShowTime>>
}