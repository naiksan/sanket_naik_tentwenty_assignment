package com.naiksan.bookmovies.data.repository

import androidx.lifecycle.LiveData
import com.naiksan.bookmovies.data.model.Seat

interface SeatsRepository {
    suspend fun getSeats(): LiveData<List<Seat>>
}