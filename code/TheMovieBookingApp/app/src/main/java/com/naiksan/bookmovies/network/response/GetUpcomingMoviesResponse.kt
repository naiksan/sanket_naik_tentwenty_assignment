package com.naiksan.bookmovies.network.response


import com.google.gson.annotations.SerializedName
import com.naiksan.bookmovies.data.model.Movie

data class GetUpcomingMoviesResponse(
    @SerializedName("results")
    val results: List<Movie>,
    @SerializedName("page")
    val page: Int,
    @SerializedName("total_results")
    val totalResults: Int,
    @SerializedName("dates")
    val dates: Dates,
    @SerializedName("total_pages")
    val totalPages: Int
) {
    data class Dates(
        @SerializedName("maximum")
        val maximum: String,
        @SerializedName("minimum")
        val minimum: String
    )
}