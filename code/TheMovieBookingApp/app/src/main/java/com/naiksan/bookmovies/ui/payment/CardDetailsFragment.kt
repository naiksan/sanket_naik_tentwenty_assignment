package com.naiksan.bookmovies.ui.payment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.naiksan.bookmovies.data.db.AppDatabase
import com.naiksan.bookmovies.data.model.Card
import com.naiksan.bookmovies.ui.base.ScopedFragment
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import com.naiksan.bookmovies.databinding.FragmentCardDetailBinding
import com.naiksan.bookmovies.internal.interfaces.OnTextChanged
import com.naiksan.bookmovies.ui.base.SharedViewModel
import com.naiksan.bookmovies.util.Validations
import com.naiksan.bookmovies.util.hideSoftKeyboard
import kotlinx.android.synthetic.main.fragment_card_detail.*
import kotlinx.coroutines.launch
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread
import org.kodein.di.generic.instance


class CardDetailsFragment : ScopedFragment(), KodeinAware {

    override val kodein by kodein()
    private lateinit var binding: FragmentCardDetailBinding
    private val viewModelFactory: CardDetailsViewModelFactory by instance()
    private lateinit var viewModel: CardDetailsViewModel
    private lateinit var sharedViewModel: SharedViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentCardDetailBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CardDetailsViewModel::class.java)
        sharedViewModel = ViewModelProviders.of(requireActivity()).get(SharedViewModel::class.java)
        bindUI()
    }

    override fun onPause() {
        super.onPause()
        requireContext().hideSoftKeyboard(cardNumber1)
    }

    private fun bindUI() {
        launch {
            val card = viewModel.card.await()
            card.observe(this@CardDetailsFragment, Observer { savedCard ->
                savedCard?.let { card ->
                    if (card.number.length >= 16) {
                        cardNumber1.setText(card.number.substring(0, 4))
                        cardNumber2.setText(card.number.substring(4, 8))
                        cardNumber3.setText(card.number.substring(8, 12))
                        cardNumber4.setText(card.number.substring(12, 16))
                    }
                    expiryMonth.setText(card.monthExpiry)
                    expiryYear.setText(card.yearExpiry)
                    //cvv.setText(card.cvv)
                    cardHolder.setText(card.holderName)
                }
            })
        }
        cardNumber1.addTextChangedListener(OnTextChanged {
            if (it.length == 4) {
                cardNumber2.requestFocus()
            }
        })
        cardNumber2.addTextChangedListener(OnTextChanged {
            when (it.length) {
                0 -> cardNumber1.requestFocus()
                4 -> cardNumber3.requestFocus()
            }
        })
        cardNumber3.addTextChangedListener(OnTextChanged {
            when (it.length) {
                0 -> cardNumber2.requestFocus()
                4 -> cardNumber4.requestFocus()
            }
        })
        cardNumber4.addTextChangedListener(OnTextChanged {
            when (it.length) {
                0 -> cardNumber3.requestFocus()
                4 -> expiryMonth.requestFocus()
            }
        })
        expiryMonth.addTextChangedListener(OnTextChanged {
            when (it.length) {
                0 -> cardNumber4.requestFocus()
                2 -> expiryYear.requestFocus()
            }
        })
        expiryYear.addTextChangedListener(OnTextChanged {
            when (it.length) {
                0 -> expiryMonth.requestFocus()
                2 -> cvv.requestFocus()
            }
        })
        cvv.addTextChangedListener(OnTextChanged {
            if (it.length == 3) {
                if (cardHolder.text.isNullOrEmpty()) {
                    cardHolder.requestFocus()
                } else {
                    requireContext().hideSoftKeyboard(cardNumber1)
                }
            }
        })

        binding.onProceed = View.OnClickListener {
            val cardNumber =
                "${cardNumber1.text.toString().trim()}${cardNumber2.text.toString().trim()}${cardNumber3.text.toString().trim()}${cardNumber4.text.toString().trim()}"
            val month = expiryMonth.text.toString().trim()
            val year = expiryYear.text.toString().trim()
            val cvvText = cvv.text.toString().trim()
            val holderName = cardHolder.text.toString().trim()
            when {
                !Validations.isCardNumberValid(cardNumber) -> {
                    cardNumber1.requestFocus()
                    requireContext().toast("Invalid card number")
                }

                !Validations.isExpiryMonthValid(month) -> {
                    expiryMonth.requestFocus()
                    requireContext().toast("Invalid month")
                }

                !Validations.isExpiryYearValid(year) -> {
                    expiryYear.requestFocus()
                    requireContext().toast("Invalid year")
                }

                !Validations.isCvvValid(cvvText) -> {
                    cvv.requestFocus()
                    requireContext().toast("Invalid CVV")
                }

                !Validations.isHolderNameValid(holderName) -> {
                    cardHolder.requestFocus()
                    requireContext().toast("Invalid Card holder name")
                }

                //all valid
                else -> {
                    doAsync {
                        val card = Card(
                            0,
                            cardNumber,
                            month,
                            year,
                            cvvText,
                            holderName
                        )
                        AppDatabase(requireContext()).cardDao().save(card)
                        uiThread {
                            sharedViewModel.selectCard(card)
                            requireContext().toast("Payment successful")
                            CardDetailsFragmentDirections.actionFragmentCardDetailsToFragmentTicketDetails().let {
                                findNavController().navigate(it)
                            }
                        }
                    }
                }
            }
        }
    }
}
