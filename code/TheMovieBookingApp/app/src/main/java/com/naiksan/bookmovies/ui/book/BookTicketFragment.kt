package com.naiksan.bookmovies.ui.book


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController

import com.naiksan.bookmovies.adapter.MovieListAdapter
import com.naiksan.bookmovies.adapter.ShowTimeAdapter
import com.naiksan.bookmovies.data.model.Cinema
import com.naiksan.bookmovies.data.model.Location
import com.naiksan.bookmovies.data.model.Movie
import com.naiksan.bookmovies.data.model.ShowTime
import com.naiksan.bookmovies.databinding.FragmentBookTicketBinding
import com.naiksan.bookmovies.ui.base.ScopedFragment
import com.naiksan.bookmovies.ui.base.SharedViewModel
import com.naiksan.bookmovies.ui.util.SelectSeatCountDialog
import com.naiksan.bookmovies.util.setGrid
import kotlinx.android.synthetic.main.fragment_book_ticket.*
import kotlinx.android.synthetic.main.fragment_movie_list.*
import kotlinx.coroutines.launch
import org.jetbrains.anko.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class BookTicketFragment : ScopedFragment(), KodeinAware {

    override val kodein by kodein()
    private val viewModelFactory: BookTicketViewModelFactory by instance()
    private lateinit var viewModel: BookTicketViewModel
    private lateinit var sharedViewModel: SharedViewModel
    private lateinit var adapter: ShowTimeAdapter
    private lateinit var binding: FragmentBookTicketBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentBookTicketBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(BookTicketViewModel::class.java)
        sharedViewModel = ViewModelProviders.of(requireActivity()).get(SharedViewModel::class.java)
        bindUI()
    }

    private fun bindUI() {
        binding.onProceed = View.OnClickListener {
            if (sharedViewModel.selectedShowTime.value == null) {
                requireActivity().toast("Please select Show time")
                return@OnClickListener
            }
            val dialog = SelectSeatCountDialog {
                BookTicketFragmentDirections.actionFragmentBookTicketToFragmentChooseSeat(it.count).let {
                    findNavController().navigate(it)
                }
            }
            dialog.show(fragmentManager, "counts_dialog")
        }
        fetchLocation()
    }

    private fun fetchLocation() {
        launch {
            val locations = viewModel.locations.await()
            locations.observe(this@BookTicketFragment, Observer { list ->
                val sList = getStringLocationList(list)
                val adapter = ArrayAdapter<String>(
                    requireContext(),
                    android.R.layout.simple_spinner_dropdown_item,
                    sList
                )
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                location.adapter = adapter
                location.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {

                    }

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, pos: Int, p3: Long) {
                        fetchCinemas(list[pos].id)
                        sharedViewModel.selectLocation(list[pos])
                    }
                }
            })
        }
    }

    private fun fetchCinemas(locationId: Int) {
        launch {
            val cinemas = viewModel.getCinemas(locationId).await()
            cinemas.observe(this@BookTicketFragment, Observer { list ->
                val adapter = ArrayAdapter<String>(
                    requireContext(),
                    android.R.layout.simple_spinner_dropdown_item,
                    getStringCinemasList(list)
                )
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                cinema.adapter = adapter
                cinema.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {

                    }

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, pos: Int, p3: Long) {
                        fetchShows(list[pos].id)
                        sharedViewModel.selectCinema(list[pos])
                    }
                }
            })
        }
    }

    private fun fetchShows(cinemaId: Int) {
        launch {
            val cinemas = viewModel.getShowTIme(cinemaId).await()
            cinemas.observe(this@BookTicketFragment, Observer { list ->
                adapter = ShowTimeAdapter(list) { show ->
                    sharedViewModel.selectShowTime(show)
                }
                showList.adapter = adapter
            })
        }
    }

    private fun getStringLocationList(list: List<Location>?): List<String> {
        val sList: ArrayList<String> = ArrayList()
        list?.forEach {
            sList.add(it.name)
        }
        return sList
    }

    private fun getStringCinemasList(list: List<Cinema>?): List<String> {
        val sList: ArrayList<String> = ArrayList()
        list?.forEach {
            sList.add(it.name)
        }
        return sList
    }

}