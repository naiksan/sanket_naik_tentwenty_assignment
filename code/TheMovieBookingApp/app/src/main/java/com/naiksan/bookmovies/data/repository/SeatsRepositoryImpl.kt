package com.naiksan.bookmovies.data.repository

import androidx.lifecycle.LiveData
import com.naiksan.bookmovies.data.db.dao.SeatDao
import com.naiksan.bookmovies.data.model.Seat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SeatsRepositoryImpl(
    private val seatDao: SeatDao
) : SeatsRepository {
    override suspend fun getSeats(): LiveData<List<Seat>> {
        return withContext(Dispatchers.IO) {
            return@withContext seatDao.get()
        }
    }

}