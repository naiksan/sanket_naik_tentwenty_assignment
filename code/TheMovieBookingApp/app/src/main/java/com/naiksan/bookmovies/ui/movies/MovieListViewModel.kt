package com.naiksan.bookmovies.ui.movies

import androidx.lifecycle.ViewModel
import com.naiksan.bookmovies.data.repository.MoviesRepository
import com.naiksan.bookmovies.internal.lazyDeferred

class MovieListViewModel(
    private val moviesRepository: MoviesRepository
) : ViewModel() {
    val movies by lazyDeferred {
        moviesRepository.getUpcomingMovies()
    }
}