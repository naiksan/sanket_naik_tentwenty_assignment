package com.naiksan.bookmovies.ui.account

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.naiksan.bookmovies.R
import com.naiksan.bookmovies.data.db.AppDatabase
import com.naiksan.bookmovies.data.model.Ticket
import com.naiksan.bookmovies.ui.base.ScopedFragment
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import com.naiksan.bookmovies.databinding.FragmentAccountBinding
import com.naiksan.bookmovies.ui.base.SharedViewModel
import org.jetbrains.anko.doAsync


class AccountFragment : ScopedFragment(), KodeinAware {

    override val kodein by kodein()
    private lateinit var binding: FragmentAccountBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentAccountBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        bindUI()
    }

    private fun bindUI() {

    }
}
