package com.naiksan.bookmovies.data.repository

import androidx.lifecycle.LiveData
import com.naiksan.bookmovies.data.model.Ticket

interface TicketRepository {
    suspend fun getTicket(): LiveData<Ticket>
}