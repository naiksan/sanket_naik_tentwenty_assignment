package com.naiksan.bookmovies.ui.seats

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.naiksan.bookmovies.data.repository.SeatsRepository

class SeatListViewModelFactory(
    private val seatRepository: SeatsRepository
) : ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SeatListViewModel(seatRepository) as T
    }
}