package com.naiksan.bookmovies.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.naiksan.bookmovies.data.model.ShowTime
import com.naiksan.bookmovies.databinding.ItemShowTimeBinding

class ShowTimeAdapter(
    private val shows: List<ShowTime>,
    private val listener: (ShowTime) -> Unit
) : RecyclerView.Adapter<ShowTimeAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            ItemShowTimeBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }


    override fun getItemCount(): Int = shows.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(createItemClickListener(position), item)
    }

    private fun getItem(position: Int): ShowTime = shows[position]

    private fun createItemClickListener(pos: Int): View.OnClickListener = View.OnClickListener { view ->
        shows.forEach {
            if (it.isSelected)
                it.isSelected = false
        }
        if (!shows[pos].isSelected) {
            shows[pos].isSelected = true
        }
        listener(shows[pos])
        notifyDataSetChanged()
    }


    class MyViewHolder(
        val binding: ItemShowTimeBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(listener: View.OnClickListener, itm: ShowTime) {
            binding.apply {
                itemClickListener = listener
                show = itm
                executePendingBindings()
            }
        }
    }

}