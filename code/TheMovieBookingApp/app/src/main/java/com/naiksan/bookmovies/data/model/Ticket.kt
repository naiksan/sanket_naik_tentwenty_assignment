package com.naiksan.bookmovies.data.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tickets")
data class Ticket(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ticket_id")
    val id: Int = 0,

    var ticketId: String?,
    var seats: String?,
    @Embedded
    var showTime: ShowTime?,
    @Embedded
    var cinema: Cinema?,
    @Embedded
    var location: Location?,
    @Embedded
    var movie: Movie?,
    @Embedded
    var card: Card?,

    val createdAt: Long = System.currentTimeMillis()
)