package com.naiksan.bookmovies.ui.seats

import androidx.lifecycle.ViewModel
import com.naiksan.bookmovies.data.repository.MoviesRepository
import com.naiksan.bookmovies.data.repository.SeatsRepository
import com.naiksan.bookmovies.internal.lazyDeferred

class SeatListViewModel(
    private val seatsRepository: SeatsRepository
) : ViewModel() {
    val seats by lazyDeferred {
        seatsRepository.getSeats()
    }
}