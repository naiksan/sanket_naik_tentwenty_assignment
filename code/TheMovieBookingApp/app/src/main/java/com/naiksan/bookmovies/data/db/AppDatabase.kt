package com.naiksan.bookmovies.data.db

import android.content.Context
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.naiksan.bookmovies.data.db.dao.*
import com.naiksan.bookmovies.data.model.*
import com.naiksan.bookmovies.util.Constants
import com.naiksan.bookmovies.workers.SeedDatabaseWorker

@Database(
    entities = [Seat::class, Movie::class, Location::class, Cinema::class, ShowTime::class, Card::class, Ticket::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun seatDao(): SeatDao
    abstract fun movieDao(): MoviesDao

    abstract fun locationDao(): LocationDao
    abstract fun cinemaDao(): CinemaDao
    abstract fun showTimeDao(): ShowTimeDao
    abstract fun cardDao(): CardDao
    abstract fun ticketDao(): TicketsDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null
        private val LOCK = Any()
        operator fun invoke(context: Context): AppDatabase {
            return instance ?: synchronized(LOCK) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, Constants.DB_NAME)
                .fallbackToDestructiveMigration()
                .addCallback(object : RoomDatabase.Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        //for seeding the db
                        val request = OneTimeWorkRequestBuilder<SeedDatabaseWorker>().build()
                        WorkManager.getInstance().enqueue(request)
                    }
                })
                .build()
        }
    }
}
