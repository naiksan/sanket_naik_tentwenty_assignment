package com.naiksan.bookmovies.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.recyclerview.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.naiksan.bookmovies.R
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


fun ImageView.loadImage(url: String?) {
    if (url.isNullOrEmpty()) return
    Glide.with(this.context)
        .setDefaultRequestOptions(RequestOptions().apply { placeholder(R.color.colorImagePlaceholder) })
        .load(url)
        .into(this)
}

fun ImageView.loadImage(bitmap: Bitmap) {
    Glide.with(this.context)
        .setDefaultRequestOptions(RequestOptions().apply { placeholder(R.color.colorImagePlaceholder) })
        .load(bitmap)
        .into(this)
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.enable() {
    this.isEnabled = true
}

fun View.disable() {
    this.isEnabled = false
}

fun Int.dpToPx(context: Context): Int {
    val displayMetrics = context.resources.displayMetrics
    return Math.round(this * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
}

inline fun <reified T : Any> Activity.launchActivity(bundle: Bundle? = null, noinline init: Intent.() -> Unit = {}) {
    val intent = newIntent<T>(this)
    intent.init()
    startActivity(intent)
}

inline fun <reified T : Any> newIntent(context: Context): Intent = Intent(context, T::class.java)


//recyclerview utils
enum class Orientation { H, V }

internal fun RecyclerView.setLinearLayoutManager(context: Context, orientation: Orientation = Orientation.V) {
    if (orientation == Orientation.V)
        layoutManager = LinearLayoutManager(context)
    else
        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
}

internal fun RecyclerView.setItemAnimator() {
    itemAnimator = DefaultItemAnimator()
}

internal fun RecyclerView.enableSeparator(context: Context) {
    addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL))
}

internal fun RecyclerView.setVertical(context: Context) {
    layoutManager = LinearLayoutManager(context)
    itemAnimator = DefaultItemAnimator()
    //setHasFixedSize(true)
}

internal fun RecyclerView.setHorizontal(context: Context) {
    layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    itemAnimator = DefaultItemAnimator()
    setHasFixedSize(true)
}

internal fun RecyclerView.setGrid(context: Context, span: Int) {
    layoutManager = GridLayoutManager(context, span)
    itemAnimator = DefaultItemAnimator()
    //setHasFixedSize(true)
}

internal fun Int.noZero(): String {
    return if (this == 0) "-"
    else this.toString()
}

internal fun String.toRelativeTime(): String {
    try {
        val dt = DateTime(this, DateTimeZone.UTC)

        val toFormat = SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.ENGLISH)
        val formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        val currentTime: Long = System.currentTimeMillis() / 1000
        val oldTime: Long = dt.millis / 1000
        val diff = currentTime - oldTime
        val secs = diff.toInt()
        val mins = diff.toInt() / 60
        val hours = diff.toInt() / (60 * 60)

        return if (secs < 60)
            "Just now"
        else if (mins < 60)
            "$mins min${if (mins == 1) "" else "s"} ago"
        else if (hours < 24)
            "$hours hour${if (hours == 1) "" else "s"} ago"
        else
            toFormat.format(Date(oldTime * 1000))
    } catch (e: ParseException) {
        return this
    }
}

internal fun String.toDisplayDate(): String {
    val fromFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
    val toFormat = SimpleDateFormat("dd MMM  yyyy", Locale.ENGLISH)
    return try {
        val date: Date = fromFormat.parse(this)
        toFormat.format(date)
    } catch (e: ParseException) {
        this
    }
}

internal fun Long.toDisplayDate(): String {
    val toFormat = SimpleDateFormat("dd MMM  yyyy", Locale.ENGLISH)
    return try {
        val date =Date(this)
        toFormat.format(date)
    } catch (e: ParseException) {
        ""
    }
}

fun Context.hideSoftKeyboard(view: View) {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
    imm?.hideSoftInputFromWindow(view.windowToken, 0)
}