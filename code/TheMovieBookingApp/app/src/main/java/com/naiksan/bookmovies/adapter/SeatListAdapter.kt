package com.naiksan.bookmovies.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.naiksan.bookmovies.data.model.Seat
import com.naiksan.bookmovies.databinding.ItemSeatBinding
import com.naiksan.bookmovies.util.Constants

class SeatListAdapter(
    private val seats: List<Seat>,
    private val listener: Listener
) : RecyclerView.Adapter<SeatListAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            ItemSeatBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }


    override fun getItemCount(): Int = seats.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(createItemClickListener(position), item)
    }

    private fun getItem(position: Int): Seat = seats[position]

    private fun createItemClickListener(pos: Int): View.OnClickListener = View.OnClickListener { view ->
        listener.onItemClick(pos)
    }

    fun getSelectedNumbers(): String {
        var numbers = ""
        seats
            .forEach {
                if (it.state == Constants.Seat.SELECTED)
                    numbers += it.seatNo + " | "
            }
        return numbers
    }


    class MyViewHolder(
        val binding: ItemSeatBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(listener: View.OnClickListener, itm: Seat) {
            binding.apply {
                itemClickListener = listener
                seat = itm
                executePendingBindings()
            }
        }
    }

    interface Listener {
        fun onItemClick(pos: Int)
    }
}