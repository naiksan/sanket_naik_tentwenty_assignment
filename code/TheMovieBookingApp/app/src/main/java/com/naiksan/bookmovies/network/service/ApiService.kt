package com.naiksan.bookmovies.network.service

import androidx.lifecycle.LiveData
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.naiksan.bookmovies.network.response.GetUpcomingMoviesResponse
import com.naiksan.bookmovies.util.Constants
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface ApiService {

    @GET("/3/movie/upcoming")
    fun getUpcomingMoviesAsync(
        @Query("api_key") apiKey: String = Constants.Service.API_KEY
    ): Deferred<GetUpcomingMoviesResponse>

    companion object {

        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor
        ): ApiService {
            val clientBuilder = OkHttpClient.Builder().apply {
                connectTimeout(Constants.Service.CONNECT, TimeUnit.MILLISECONDS)
                readTimeout(Constants.Service.READ, TimeUnit.MILLISECONDS)
                writeTimeout(Constants.Service.WRITE, TimeUnit.MILLISECONDS)
                addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
                addInterceptor(connectivityInterceptor)
                addNetworkInterceptor ani@{ chain ->
                    val request = chain.request().newBuilder()
                        .build()
                    return@ani chain.proceed(request)
                }
            }
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .baseUrl(Constants.Service.BASE_URL)
                .client(clientBuilder.build())
                .build()
            return retrofit.create(ApiService::class.java)
        }
    }

}