package com.naiksan.bookmovies.ui.book

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.naiksan.bookmovies.data.model.Cinema
import com.naiksan.bookmovies.data.model.ShowTime
import com.naiksan.bookmovies.data.repository.BookTicketRepository
import com.naiksan.bookmovies.data.repository.MoviesRepository
import com.naiksan.bookmovies.internal.lazyDeferred
import kotlinx.coroutines.Deferred

class BookTicketViewModel(
    private val bookTicketRepository: BookTicketRepository
) : ViewModel() {
    val locations by lazyDeferred {
        bookTicketRepository.getLocations()
    }

    fun getCinemas(locationId: Int): Deferred<LiveData<List<Cinema>>> {
        val cinemas by lazyDeferred {
            bookTicketRepository.getCinemas(locationId)
        }
        return cinemas
    }

    fun getShowTIme(cinemaId: Int): Deferred<LiveData<List<ShowTime>>> {
        val shows by lazyDeferred {
            return@lazyDeferred bookTicketRepository.getShowTime(cinemaId)
        }
        return shows
    }
}