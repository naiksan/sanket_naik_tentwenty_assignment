package com.naiksan.bookmovies.ui.base

import androidx.lifecycle.*
import com.naiksan.bookmovies.data.model.*

class SharedViewModel(
) : ViewModel() {
    val selectedSeatNos = MutableLiveData<String>()

    fun selectSeat(item: String) {
        selectedSeatNos.value = item
    }

    val selectedMovie = MutableLiveData<Movie>()

    fun selectMovie(item: Movie) {
        selectedMovie.value = item
    }

    val selectedCard = MutableLiveData<Card>()

    fun selectCard(item: Card) {
        selectedCard.value = item
    }

    val selectedLocation = MutableLiveData<Location>()

    fun selectLocation(item: Location) {
        selectedLocation.value = item
    }

    val selectedCinema = MutableLiveData<Cinema>()

    fun selectCinema(item: Cinema) {
        selectedCinema.value = item
    }

    val selectedShowTime = MutableLiveData<ShowTime>()

    fun selectShowTime(item: ShowTime) {
        selectedShowTime.value = item
    }

   /* fun saveTicket(ticket: Ticket) {
        lazyDeferred {
            sharedRepository.saveTicket(ticket)
        }
    }*/

    val ticket = MutableLiveData<Ticket>()

    fun createTicket() {
        ticket.value = Ticket(
            0,
            "TID${System.currentTimeMillis() / 1000}",
            selectedSeatNos.value,
            selectedShowTime.value,
            selectedCinema.value,
            selectedLocation.value,
            selectedMovie.value,
            selectedCard.value
        )
    }
}