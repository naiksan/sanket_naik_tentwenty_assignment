package com.naiksan.bookmovies.adapter

import android.graphics.Paint
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.naiksan.bookmovies.R
import com.naiksan.bookmovies.util.Constants
import com.naiksan.bookmovies.util.toDisplayDate
import com.naiksan.bookmovies.util.toRelativeTime

@BindingAdapter("isGone")
fun bindIsGone(view: View, isGone: Boolean) {
    view.visibility = if (isGone) {
        View.GONE
    } else {
        View.VISIBLE
    }
}

@BindingAdapter("loadImage")
fun bindImageFromUrl(view: ImageView, imageUrl: String?) {
    if (!imageUrl.isNullOrEmpty()) {
        Glide.with(view.context)
            .load(imageUrl)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(view)
    }
}

@BindingAdapter("loadImageUrl")
fun bindImageFromUrl2(view: ImageView, imageUrl: String?) {
    if (!imageUrl.isNullOrEmpty()) {
        Glide.with(view.context)
            .load(imageUrl)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(view)
    }
}

@BindingAdapter("loadPoster")
fun bindLoadPoster(view: ImageView, imageUrl: String?) {
    if (!imageUrl.isNullOrEmpty()) {
        Glide.with(view.context)
            .load("http://image.tmdb.org/t/p/w185$imageUrl")
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(view)
    }
}

@BindingAdapter("loadRoundImage")
fun bindImageFromDrawable(view: ImageView, imageUrl: String?) {
    if (!imageUrl.isNullOrEmpty()) {
        Glide.with(view.context)
            .load(imageUrl)
            .apply(RequestOptions().circleCrop())
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(view)
    }
}

@BindingAdapter("loadDrawableImage")
fun bindImageFromUrlRound(view: ImageView, drawable: Int) {
    Glide.with(view.context)
        .load(drawable)
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(view)
}

@BindingAdapter("setStateBg")
fun bindSetStateBg(view: ImageView, state: String) {
    when (state) {
        Constants.Seat.AVAILABLE -> view.setImageResource(R.drawable.im_seat_available)
        Constants.Seat.BOOKED -> view.setImageResource(R.drawable.im_seat_booked)
        else -> view.setImageResource(R.drawable.im_seat_selected)
    }
}

@BindingAdapter("setSelectedBg")
fun bindSetSelectedBg(view: FrameLayout, selected: Boolean) {
    view.setBackgroundResource(if (selected) R.drawable.bg_edittext_seleted else R.drawable.bg_edittext)
}

@BindingAdapter("isChecked")
fun bindIsChecked(view: ImageButton, count: Int) {
    view.isSelected = count == 1
}

@BindingAdapter("goneIfNull")
fun bindGoneIfNull(view: View, gone: String?) {
    view.visibility = if (gone == null) {
        View.GONE
    } else {
        View.VISIBLE
    }
}

@BindingAdapter("strikeText")
fun bindStrikeText(view: TextView, value: String?) {
    view.paintFlags = view.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
}

@BindingAdapter("textSafe")
fun bindTextSafe(view: TextView, text: Any) {
    view.text = text.toString()
}

@BindingAdapter("toDate")
fun bindToDate(view: TextView, text: String?) {
    text?.let {
        view.text = text.toRelativeTime()
    }
}

@BindingAdapter("longToDate")
fun bindLongToDate(view: TextView, timestamp: Long) {
    view.text = timestamp.toDisplayDate()
}

@BindingAdapter("showSelected")
fun bindShowSelected(view: FrameLayout, selected: Boolean) {
    view.isSelected = selected
}

@BindingAdapter("checkedIfOne")
fun bindCheckedIfOne(view: ImageButton, value: Int) {
    view.isSelected = value == 1
}