package com.naiksan.bookmovies.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cards")
data class Card(
    @PrimaryKey
    @ColumnInfo(name = "card_id")
    val id: Int = 0,
    val number: String,
    val monthExpiry: String,
    val yearExpiry: String,
    val cvv: String,
    val holderName: String
)