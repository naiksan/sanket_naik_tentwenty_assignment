package com.naiksan.bookmovies.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "showtime")
data class ShowTime(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "show_id")
    val id: Int,
    val cinemaId: Int,
    @ColumnInfo(name = "show_name")
    val name: String
){
    var isSelected: Boolean = false
}