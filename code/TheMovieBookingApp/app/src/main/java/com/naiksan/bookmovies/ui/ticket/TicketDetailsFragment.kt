package com.naiksan.bookmovies.ui.ticket

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.naiksan.bookmovies.data.db.AppDatabase
import com.naiksan.bookmovies.data.model.Ticket
import com.naiksan.bookmovies.ui.base.ScopedFragment
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import com.naiksan.bookmovies.databinding.FragmentTicketDetailBinding
import com.naiksan.bookmovies.ui.base.SharedViewModel
import org.jetbrains.anko.doAsync


class TicketDetailsFragment : ScopedFragment(), KodeinAware {

    override val kodein by kodein()
    private lateinit var binding: FragmentTicketDetailBinding
    private lateinit var sharedViewModel: SharedViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentTicketDetailBinding.inflate(inflater, container, false)
        sharedViewModel = ViewModelProviders.of(requireActivity()).get(SharedViewModel::class.java)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        bindUI()
    }

    private fun bindUI() {
        sharedViewModel.createTicket()
        sharedViewModel.ticket.observe(this@TicketDetailsFragment, Observer { ticket ->
            doAsync {
                AppDatabase(requireContext()).ticketDao().save(ticket)
            }
            binding.ticket = ticket
        })
    }
}
