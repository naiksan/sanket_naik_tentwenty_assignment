package com.naiksan.bookmovies.data.prefs

import android.content.Context
import android.content.SharedPreferences

class AppPrefs private constructor() {

    companion object {

        @Volatile
        private lateinit var sharedPrefs: SharedPreferences
        private var appPrefs: AppPrefs? = null

        fun getInstance(context: Context): AppPrefs =
            appPrefs ?: synchronized(this) {
                appPrefs ?: AppPrefs().also {
                    appPrefs = it
                    sharedPrefs = context.getSharedPreferences("tmdb_prefs", Context.MODE_PRIVATE)
                }
            }
    }

    fun setValue(key: String, value: String) {
        sharedPrefs.edit().putString(key, value).apply()
    }

    fun getValue(key: String): String? {
        return sharedPrefs.getString(key, null)
    }
}