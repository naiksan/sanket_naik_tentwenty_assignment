package com.naiksan.bookmovies.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.naiksan.bookmovies.data.model.Card
import com.naiksan.bookmovies.data.model.Cinema
import com.naiksan.bookmovies.data.model.ShowTime


@Dao
interface CardDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(card: Card)

    @Query("SELECT * FROM cards")
    fun get(): LiveData<Card>

}