package com.naiksan.bookmovies.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.naiksan.bookmovies.data.model.Location
import com.naiksan.bookmovies.data.model.ShowTime

@Dao
interface LocationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(location: Location)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(location: List<Location>)

    @Query("SELECT * FROM location")
    fun get(): LiveData<List<Location>>

}